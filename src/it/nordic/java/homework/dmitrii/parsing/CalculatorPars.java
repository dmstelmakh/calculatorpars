package it.nordic.java.homework.dmitrii.parsing;

import javax.swing.*;
import java.util.*;

public class CalculatorPars {
    public static void main(String[] args) {
        new CalculatorPars().execute();
    }

    class Operand {
        int getValue() {
            return 0;
        }
    }

    class MathAction {
        int compute(Operand o1, Operand o2) {
            return 0;
        }
    }

    enum Operation {
        ADD, SUB, MLT, DEL;
    }

    interface Operand {
        int compute();
    }

    class OperandConst implements Operand {
        private int value;

        @Override
        public int compute() {
            return value;
        }
    }

    class OperandVariable implements Operand {
        private String nameOfVariable;

        @Override
        public int compute() {
            return extraValue(nameOfVariable);
        }

        private int extraValue(String nameOfVariable) {
            return 0;
        }
    }

    class operndComplex implements Operand {
        private Operation act;
        private Operation op1;
        private Operation op2;

        @Override
        public int compute() {
            switch (act) {
                case ADD:
                    return op1.compute() + op2.compute();
                case DEL:
                    return op1.compute() - op2.compute();
                case MLT:
                    return op1.compute() * op2.compute();
                case SUB:
                    return op1.compute() / op2.compute();
            }
            return 0;
        }
    }

    private void execute() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите формулу");
        String formyla = scan.nextLine();
        System.out.println(formyla);
        char[] arr = formyla.toCharArray();
        List<RawOperand> operand = new ArrayList<>();
        List<Operation> actions = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            char c = arr[i];
            Map<String, Integer> vars = new HashMap<>();
            StringBuilder nameBuf = new StringBuilder();
            StringBuilder valueBuf = new StringBuilder();
            String lastVarName = null;
            switch (state) {
                case START:
                    if (Character.isAlphabetic(c)) {
                        nameBuf.append(c);
                        nextState = State.CONTINUE_VAR_OR_EQ;
                    } else {
                        error = true;
                    }
                    break;
                case CONTINUE_VAR_OR_EQ:
                    if (Character.isAlphabetic(c) || Character.isDigit(c)) {
                        nameBuf.append(c);
                        nextState = State.EXPECT_VALUE;
                    } else {
                        error = true;
                    }
                    break;
                case EXPECT_VALUE:
                    if (Character.isDigit(c)) {
                        nameBuf.append(c);
                        nextState = State.CONTINUE_VALUE_OR_DELIM;

                    } else {
                        error = true;
                    }
                    break;
                case CONTINUE_VALUE_OR_DELIM:
                    if (Character.isDigit(c)) {
                        nameBuf.append(c);
                        nextState = State.CONTINUE_VALUE_OR_DELIM;
                    } else if (c == '=') {
                        String varName = nameBuf.toString();
                        vars.put(varName, null);
                        nameBuf = new StringBuilder();
                        nextState = State.EXPECT_VALUE;

                    } else {
                        error = true;
                    }
                    if (error) {
                        System.out.println("Ошибка в позиции " + i + ", символ " + c);
                        break;
                    }
                    break;
            }
        }

        Operand root = null;
        for (int i = 0; i < actions.size(); i++) {
            Operation currentAct = actions.get(i);
//            currentAct.compute(operand.get(i), operand.get(i + 1));
            //заполнение root
        }
        root.compute();
    }


//        for (int i = 0; i < actions.size(); i++) {
//            MathAction currentAct = actions.get(i);
//            currentAct.compute(Operand.get(i), Operand.get(i + 1));
//        }

    }
}
