package it.nordic.java.homework.dmitrii.parsing;

public enum State {
    START,
    CONTINUE_VAR_OR_EQ,
    EXPECT_VALUE,
    CONTINUE_VALUE_OR_DELIM;
}
